// **** INCLUDES *****
#include "LowPower.h" //https://github.com/rocketscream/Low-Power

#define MOTH_LED 5

void setup()
{
    // No setup is required for this library
      pinMode(MOTH_LED, OUTPUT);
      
}

void loop() 
{
  
   digitalWrite(MOTH_LED, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(3000);                       // wait for a second
  digitalWrite(MOTH_LED, LOW);    // turn the LED off by making the voltage LOW
                  
  
    // Enter power down state for 8 s with ADC and BOD module disabled
    LowPower.powerDown(SLEEP_8S, ADC_OFF, BOD_OFF);  
    
}
